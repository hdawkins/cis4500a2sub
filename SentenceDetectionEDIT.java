/*
CIS4500 F19 A2 (from A1)
Name: Hillary Dawkins
Student ID: 0717432
email: hdawkins@uoguelph.ca

A1 Preprocessing: Sentence splitting
This program takes input assumed to be text formatted with $DOC, $TITLE, and $TEXT labels, and outputs the text split into sentences,
with one sentence per line.   
The Sentence Detection uses OpenNLP tools. 
This file has been adapted from the provided example SetenceDetectionME. 
Note this solution has been modified from A1 solution.
*/

import java.util.Scanner;
import java.io.FileInputStream; 
import java.io.InputStream;  
import java.io.InputStreamReader;
import java.io.BufferedReader;

import opennlp.tools.sentdetect.SentenceDetectorME; 
import opennlp.tools.sentdetect.SentenceModel;  

public class SentenceDetectionEDIT { 
   
    //reads in the string by line from the given input
    public static String readString(String arg) throws Exception {
        InputStream infile = new FileInputStream(arg);
        BufferedReader buf = new BufferedReader(new InputStreamReader(infile));
        StringBuilder sb = new StringBuilder();
        String line = buf.readLine();
        while( line != null ) {
            sb.append(line + " ");
            line = buf.readLine();
        }
        return sb.toString();  
    }
  
    public static void main(String args[]) throws Exception {    
        //Load sentence detector model 
        InputStream modelData = new FileInputStream("OpenNLP_models/en-sent.bin"); 
        SentenceModel model = new SentenceModel(modelData); 
       
        //Instantiate SentenceDetectorME 
        SentenceDetectorME detector = new SentenceDetectorME(model);  
    
        //Print the sentences 
	StringBuilder sb = new StringBuilder();
	Scanner input = new Scanner(System.in);
        while (input.hasNextLine()) {
	
	    //read input one line at a time
	    String line = input.nextLine();
	
	    // If the line is one of the labels, do not process, print line as it is
	    if(line.contains("$DOC") || line.contains("$TEXT") ){
	        //write the current input text string to file (split into sentences, one per line), empty current build
		String sentences[] = detector.sentDetect(sb.toString());
		for(String sent: sentences){
		    System.out.println(sent);
		}
		sb.setLength(0);
		
		//then write the current label to file
	        System.out.print(line);
	        System.out.println();
; 
	    }
	    else if(line.contains("$TITLE")){
	        //print line as it is, nothing is currently in string build
		System.out.print(line);
	        System.out.println();
	    }
	    else{
	        //otherwise, keep building a string of input text (either title or text body)    
		sb.append(line + " ");
	    }
        }
	//after end of input is reached, we still need to write the last document text to output file 
	String sentences[] = detector.sentDetect(sb.toString());
	for(String sent: sentences){
	    System.out.println(sent);
	}
    }
}
