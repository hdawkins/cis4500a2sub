/*
CIS4500 F19 A2
Name: Hillary Dawkins
Student ID: 0717432
email: hdawkins@uoguelph.ca

PART 3: Retriever (Online processing)
This program takes the 3 text documents that describe the inverted file (dictionary.txt, postings.txt, docids.txt),
and implements an information retrieval system. The user can enter a query, until "quit" or "q" is entered.
For each query, the inner product similarity measure is computed for each document, and the top 10 results are displayed.
This program needs access to the list of stop words contained in a file named "stopwords.txt". 
*/

import java.util.Scanner;
import opennlp.tools.stemmer.PorterStemmer;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.io.File;
import java.util.TreeMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Comparator;
import java.text.DecimalFormat;

public class Retriever { 
  
   //format decimal output
   private static DecimalFormat df2 = new DecimalFormat("#.##");

   //get a column from muli-column array
   public static String[] getColumn(String[][] array, int index){
        String[] column = new String[array.length]; 
        for(int i=0; i<column.length; i++){
            column[i] = array[i][index];
        }
        return column;
    }
    
    // sort a 2D double array based on value in column indexed by col
    public static void sortbyColumn(double arr[][], int col) 
    { 
        // Using built-in sort function Arrays.sort 
        Arrays.sort(arr, new Comparator<double[]>() { 
            
          @Override              
          // Compare values according to columns 
          public int compare(final double[] entry1,  
                             final double[] entry2) { 
  
            // descending order
            if (entry1[col] < entry2[col]) 
                return 1; 
            else if (entry1[col] > entry2[col])
                return -1;
	    else
	        return 0; 
          } 
        });  
    }
   
   public static void main(String args[]) throws Exception{  
   
   //set up some definitions for query preprocessing
   //Definitions of words we want to filter
   String integers = "[+-]?[0-9]+";
   String reals = "[+-]?([0-9]*\".\"[0-9]+)";
   String punct = "[\\ \" ^ - \"+\" =~!@#$%&*:;'<>?,./ ()]+";
   //get a list of stop words, from stopwords.txt
   Scanner stopwords_in = new Scanner(new File ("stopwords.txt"));
   String word = "";
   List<String> stopwords = new ArrayList<String>();
   while (stopwords_in.hasNextLine()){
       word = stopwords_in.nextLine();
       stopwords.add(word);
   }
   //we will also need a stemmer to process query
   PorterStemmer stemmer = new PorterStemmer();
   
   //load docids file into array (array of DocID objects)
   Scanner docids_in = new Scanner(new File ("docids.txt"));
   //get number of documents from the first line, this assumes docids was formatted correctly
   int numDocs = Integer.parseInt(docids_in.nextLine()); 
   DocID [] docids = new DocID[numDocs];
   //load all other lines into the docids array - format: docid, lineNum, title
   int i = 0;
   while(docids_in.hasNext()){
       String [] line = docids_in.nextLine().split(" ",3);
       docids[i] = new DocID( line[0], line[2], Integer.parseInt(line[1]));
       i++;
   }
   
   //load postings file into array (array of PostingEntry objects)
   Scanner postings_in = new Scanner(new File ("postings.txt"));
   //get number of entries from the first line
   int numEntries = Integer.parseInt(postings_in.nextLine()); 
   PostingEntry [] postings = new PostingEntry[numEntries];
   //load all other lines into the postings array - format: did, tf
   i = 0;
   while(postings_in.hasNext()){
       String [] line = postings_in.nextLine().split(" ");
       postings[i] = new PostingEntry( Integer.parseInt(line[0]), Integer.parseInt(line[1])) ;
       i++;
   }
   
   //load dictionary file into array (array of DictEntry objects)
   Scanner dict_in = new Scanner(new File ("dictionary.txt"));
   //get number of stems from the first line
   int numStems = Integer.parseInt(dict_in.nextLine()); 
   DictEntry [] dictionary = new DictEntry[numStems];
   String[] unique_stems = new String[numStems];
   //load all other lines into the dictionary array - format: stem, offset (converted from df)
   i = 0;
   int offset = 0; //the offset for each stem is the sum of all previous df's in the array
   while(dict_in.hasNext()){
       String[] line = (dict_in.nextLine()).split(" ");
       dictionary[i] = new DictEntry(line[0], offset); //the first offset is 0     
       unique_stems[i] = line[0];
       offset = offset + Integer.parseInt(line[1]); //increment offset by the current df
       i++;
   }
   
   //enter a command prompt loop to obtain query from user and display results, until "q" or "quit"
   Scanner command = new Scanner(System.in);
   String user_input = "";
   while(!(user_input.equalsIgnoreCase("quit") || user_input.equalsIgnoreCase("q")))
   { 
       //get query 
       System.out.println("Enter query: ");
       user_input = command.nextLine();
       
       //exit if quit or q is encountered
       if(user_input.equalsIgnoreCase("quit") || user_input.equalsIgnoreCase("q")){
           System.out.println("Goodbye!");
	   break;
       }
       else{
           //otherwise process the query, compute similarities, and show results
	   
	   //processing query:
	   // i) convert to lowercase
	   user_input = user_input.toLowerCase();
	   // ii) tokenize based on whitespace only - get query terms
	   String[] query_terms = user_input.split("[ \t]+");
	   // iii) Check if the query term is a number, punctuation or stop word - if not, stem and add to query stems TreeMap
	   // TreeMap keeps track of unique stems in the query mapped to frequency (tf)
	   TreeMap<String, Integer> query_stems = new TreeMap<String, Integer>();
	   for (String term : query_terms){
	        if (term.matches(integers) || term.matches(reals) || term.matches(punct) || stopwords.contains(term)) {
                    continue;
		}
		//if term is not int, real, punct, or stop word, add its stem
		String stem = stemmer.stem(term);
		if(query_stems.containsKey(stem)){
		    //increment the term frequency
		    query_stems.put(stem, query_stems.get(stem)+1);
		}
		else{
		    //add the new stem with tf = 1
		    query_stems.put(stem, 1);
		}		
	   }
	   //now we have a map of unique query stems with term frequencies
	   
	   //compute similarities
	   //similarity is indexed by did, all initialized to 0
	   //keeping index (did) in similarity array (first col) so that is can be sorted later, and did preserved
	   //second col is the computed similarity measure
	   double[][] similarity = new double[numDocs][2];
	   int k = 0;
	   for (double[] row: similarity){
               row[0] = k;
	       row[1] = 0.0;
	       k++;
	   }
	   
	   //for each unique stem in the query, update the similarity measure array 
	   Set<String> keys = query_stems.keySet();
	   Iterator<String> iter = keys.iterator();
	   while (iter.hasNext()){
	       String key = iter.next(); //the key is the current stemmed query term being processed
	       
	       //get df and offset from the dictionary array
	       int dict_index = Arrays.binarySearch(unique_stems,key); //use binary search on sorted dictionary stems, get dict index
	       if(dict_index < 0){ //the query is not in the dictionary
	           continue; //it doesn't contribute to similarity measures
	       }
	       int term_offset = dictionary[dict_index].offset;
	       int term_df = 0; 
	       if(dict_index == (numStems-1)){
	           //we are at the last element of dict_entries array, then get df from length(postings) minus current offset
		   term_df = numEntries - term_offset;
	       }
	       else{
	           term_df = dictionary[dict_index+1].offset - term_offset; //otherwise we can use next offset - current offset
	       }
	       
	       //compute w_t(q) - the query weight for this term
	       // w_t(q) = tf(q) * log (N/df(t)) (the term freq in the query * log(numdoc/ doc freq of term in collection)
	       //NOTE : assuming log base 2
	       double query_weight = query_stems.get(key) * Math.log((double)numDocs/term_df)/Math.log(2.0); //using log base 2
	       
	       //for each document that this term appears in, get tf from posting file, compute document weight
	       for(int j = 0; j<term_df; j++){
	           //the first relevant document is in the posting array at index given by offset
		   //get the did and the tf for this document
		   int doc_did = postings[term_offset+j].did;
		   int doc_tf = postings[term_offset+j].tf;
		   //compute the document weight for this term
		   double document_weight = (double)doc_tf * Math.log((double)numDocs/term_df)/Math.log(2.0); //using log base 2
		   //update the similarity measure for this document (indexed by did) with the inner product term (query_weight*doc_weight)
		   similarity[doc_did][1] = similarity[doc_did][1] + query_weight*document_weight;
	       }	       
	   }
	   //we now have an array of all the similarity measures, indexed by did
	   
	   //display the top 10 (highest sim values) results to the user (sim value, docid, title)
	   //sort similarity matrix by similarity value
	   sortbyColumn(similarity, 1);
	   //display top 10 results (recall docids index by did (sim first col))
	   for(int t=0; t<10; t++){
	       System.out.println("SV: "+df2.format(similarity[t][1])+" docid: "+docids[(int)similarity[t][0]].docid+" title: "+docids[(int)similarity[t][0]].title);
	   }
	   
	   
       }

   }
   
   
   } 
} 

// A posting entry for a particular stem contains the document id (did - an int), and the term frequency
class PostingEntry{
    int did;
    int tf;
    PostingEntry(int did, int tf){
        this.did = did;
	this.tf = tf;
    }
}

// For each document in the collection, we will keep the docid (a string ex. LA018543-48), the title, and starting line number
class DocID{
    String docid;
    String title;
    int lineNum;
    DocID(String docid, String title, int lineNum){
        this.docid = docid;
	this.title = title;
	this.lineNum = lineNum;
    }
}

// For stem in the dictionary file, we will keep stem and its offset (converted from document freq) to index the postings array
class DictEntry{
    String stem;
    int offset;
    DictEntry(String stem, int offset){
        this.stem = stem;
	this.offset = offset;
    }
}

