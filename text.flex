/*
CIS4500 F19 A2 (from A1)
Name: Hillary Dawkins
Student ID: 0717432
email: hdawkins@uoguelph.ca

File Name: text.flex
JFlex specification for a text file where we want tokens of type:
LABEL, WORD, NUMBER, NEWLINE, HYPHENATED, ASPOSTROPHIZED
This file has been adapted from the example tiny.flex file provided.
*/
   
%%
   
%class Lexer
%type Token
%line
%column
    
%eofval{
  return null;
%eofval};


/* A line terminator is a \r (carriage return), \n (line feed), or
   \r\n. */
LineTerminator = \r|\n|\r\n
   
/* White space is a space, tab, or form feed. */
WhiteSpace     = [ \t\f]

/* LABEL - the three common labels */
/* NOTE: Decided to keep the label ID after $DOC as part of the label token, since it really isn't a word or anything else */
Label = "$DOC".* |"$TITLE"|"$TEXT" 

/* WORD - string of letters (at least one letter) and digits (zero or more) in any order */
word = [a-zA-Z0-9]*[a-zA-Z]+[a-zA-Z0-9]*

/* Number - an integer or real number with or without a minus sign or positive sign*/
/* An integer is one or more digits 0-9 - it was decided to allow leading 0's in the case  
   of typeset numbers (i.e. 001 will be one number token, not split), since this seems reasonable (e.g. Chapter 01) */ 
/* A real number is zero or more digits 0-9 followed by "." followed by atleast one digit 0-9 (e.g. 1. not a real, but int then punctuation). 
   Note this will include .0 .00 .000000 etc as a real - still seems better to treat these as numbers than splitting, so it is allowed */
integer = [+-]?[0-9]+
real = [+-]?([0-9]*"."[0-9]+)
number = {real} | {integer}

/* Apostrophized - Any word (regular or hyphenated), or numbers, connected by one or more apostrophes */
apostrophized = ({hyphenated}|{word}|{number})"'"({hyphenated}|{word}|{number})("'"({hyphenated}|{word}|{number}))*

/* Hyphenated - Any word or number (integer or real, with +- sign) connected by one or more hyphens*/
/* ex. three-day, 3-day, day-3, father-in-law, 3.149-below will all be treated as hyphenated*/
hyphenated = ({word}|{number})"-"({word}|{number})("-"({word}|{number}))*
   
   
%%
   
/*
   This section contains regular expressions and actions, i.e. Java
   code, that will be executed when the scanner matches the associated
   regular expression. */

{number}           { return new Token(Token.NUM, yytext(), yyline, yycolumn); }
{LineTerminator}   { return new Token(Token.NEWLINE, yytext(), yyline, yycolumn); }
{Label}            { return new Token(Token.LABEL, yytext(), yyline, yycolumn); }  
{word}             { return new Token(Token.WORD, yytext(), yyline, yycolumn); } 
{apostrophized}    { return new Token(Token.APOST, yytext(), yyline, yycolumn); } 
{hyphenated}       { return new Token(Token.HYPH, yytext(), yyline, yycolumn); } 
{WhiteSpace}+      { /* skip whitespace */ }   
.                  { return new Token(Token.PUNCT, yytext(), yyline, yycolumn); } /*anything else treated as punctuation */
