/*
CIS4500 F19 A2 (from A1)
Name: Hillary Dawkins
Student ID: 0717432
email: hdawkins@uoguelph.ca

A1 Preprocessing: Tokenization
The Token class returned from the Scanner contains a type (defined by an integer value), the literal string value of the token,
and where it was found (line and column numbers).
Type is one of Punctuation, Number, Newline, Label, Word, Hyphenated, and Apostrophized; see the text.flex file for more 
details on how each type is defined.
The returned "toString" value can be used for testing (e.g. printing the token instead of just it's m_value will given you TYPE(value)).
This file was adpated from the provided example Token class.  
*/

class Token {

  public final static int PUNCT = 0;
  public final static int NUM = 1;
  public final static int NEWLINE = 2;
  public final static int LABEL = 3; 
  public final static int WORD = 4; 
  public final static int APOST = 5; 
  public final static int HYPH = 6; 

  public int m_type;
  public String m_value;
  public int m_line;
  public int m_column;
  
  Token (int type, String value, int line, int column) {
    m_type = type;
    m_value = value;
    m_line = line;
    m_column = column;
  }

  public String toString() {
    switch (m_type) {
      case NUM:
        return "NUM(" + m_value + ")";
      case NEWLINE:
        return "NEWLINE(" + m_value + ")"; 
      case LABEL:
        return "LABEL(" + m_value + ")"; 
      case WORD:
        return "WORD(" + m_value + ")"; 
      case APOST:
        return "APOST(" + m_value + ")"; 
      case HYPH:
        return "HYPH(" + m_value + ")"; 
      case PUNCT:
        return "PUNCT(" + m_value + ")";
      default:
        return "UNKNOWN(" + m_value + ")";
    }
  }
}

