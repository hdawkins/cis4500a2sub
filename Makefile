JAVAC=javac
JFLEX=jflex
CLASSPATH=opennlp-tools-1.9.1.jar:.

all: Retriever.class SentenceDetectionEDIT.class Preprocessor.class Indexer.class Token.class Lexer.class Scanner.class

Retriever.class: Retriever.java
	$(JAVAC) -cp $(CLASSPATH) $^

Preprocessor.class: Preprocessor.java
	$(JAVAC) -cp $(CLASSPATH) $^

Indexer.class: Indexer.java
	$(JAVAC) -cp $(CLASSPATH) $^

SentenceDetectionEDIT.class: SentenceDetectionEDIT.java
	$(JAVAC) -cp $(CLASSPATH) $^

Token.class: Token.java
	$(JAVAC) $^

Lexer.class: Lexer.java
	$(JAVAC) $^

Scanner.class: Scanner.java
	$(JAVAC) $^

Lexer.java: text.flex
	$(JFLEX) text.flex

clean:
	rm -f Lexer.java *.class *~

