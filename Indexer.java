/*
CIS4500 F19 A2
Name: Hillary Dawkins
Student ID: 0717432
email: hdawkins@uoguelph.ca

PART 2: Indexing (Offline processing - creating inverted files)
This program takes a stemmed input file and produces the inverted index.
The index is described by three produced text files: 
1. dictionary.txt contains the number of unique stems on the first line, followed by all unique stems 
in sorted order and the document freq seperated by a whitespace (one stem df pair per line)
2. postings.txt contains the posting entries (total number of entries on first line), followed by did tf pairs,
one per line (seperated by whitespace)
3. docids.txt contains the document id information for each document in the collection (number of documents on first line),
followed by docid starting_line_number title_string triples one per line separated by whitespace  
This file was adapted from the example TreeMap file. 
*/

import java.util.Scanner;
import java.util.TreeMap;
import java.util.Set;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Indexer {

   public static void main(String args[]) {     
      //TreeMap maps string(stem) to ArrayList of PostingEntrys
      TreeMap<String, ArrayList<PostingEntry> > map = new TreeMap<String, ArrayList<PostingEntry> >();
      
      //keep track of the current docid (will start at 0), and current line number (will start at 1)
      int current_did = -1;
      int current_lineN = 0;
      
      //DocID ArrayList for keeping track of docid, title and start line number, for all docs in collection
      ArrayList<DocID> doc_collection = new ArrayList<DocID>();
      //Use a string builder to keep track of the title for each document (which can span multiple lines in input)
      StringBuilder sb = new StringBuilder();
      //keep track of whether or not we should be recording the title, current docid and current doc_line_number
      int record_title = 0;
      String docid = "";
      int doc_line_num = 0;

      //load an input file into the TreeMap
      Scanner input = new Scanner(System.in);
      while (input.hasNextLine()) {
        current_lineN++;
        //get input from stem document, one line at a time
        String line = input.nextLine();	
	// if the line is a label, ignore - does not contribute to stem counts
	if(line.contains("$DOC") || line.contains("$TEXT") || line.contains("$TITLE")){
	    // do not process the line for the TreeMap
	    //if it's the start of a new doc, increment doc number, update DocID ArrayList
	    if(line.contains("$DOC")){
	        current_did++;
		docid = line.split("[ \t]+")[1];
		doc_line_num = current_lineN;
	    }
	    //if it's the title, start recording the title (title can span multiple lines)
	    if(line.contains("$TITLE")){
	        //record flag on
		record_title = 1;
	    }
	    //if it's the text, stop recording the title (title body between $title and $text labels)
	    if(line.contains("$TEXT")){
	        //record flag off
		record_title = 0;
		//now we've compiled the title, we can create a DocID object for the current doc and add to DocID arraylist
		DocID doc = new DocID(docid, sb.toString(), doc_line_num);
		doc_collection.add(doc);
		//reset the string builder so we can record a new title when it is encountered
		sb.setLength(0);
	    }
	}
	else
	{
	    //otherwise, split the line into tokens, and update the TreeMap for each stem as applicable
            String[] stems = line.split("[ \t]+");
            for (int i = 0; i < stems.length; i++) {
                // update stems on TreeMap
                if (map.containsKey(stems[i])){ 
	            //if the stem is already in the TreeMap, get the corresponding ArrayList and update it
		    ArrayList<PostingEntry> tempList = map.get(stems[i]); 
		    //check if the latest did is the current did
		    int latest_did = tempList.get(tempList.size()-1).did;
	            //if current_did matches latest_did (we are still in the same document), increment tf on current entry
		    if(latest_did == current_did){
			PostingEntry temp_entry = tempList.get(tempList.size()-1);
			temp_entry.tf = temp_entry.tf + 1;
			tempList.set(tempList.size()-1, temp_entry);
		    }
		    else{
		        //otherwise this is the first time this stem appears in this document, create new entry and add to list (tf=1)
			PostingEntry new_entry = new PostingEntry(current_did, 1);
			tempList.add(new_entry);
		    }
		    //rewrite tempList to TreeMap
		    map.put(stems[i], tempList);
	        }
                else{
		    //we have observed a new stem
		    //create a new PostingEntry, add to new ArrayList, add to the TreeMap
		    PostingEntry new_entry = new PostingEntry(current_did, 1);
		    ArrayList<PostingEntry> new_list = new ArrayList<PostingEntry>();
		    new_list.add(new_entry);
	            map.put(stems[i], new_list);
		}
            }
	    
	    //for every line that isn't a label, we also append to the title, if record flag is on
	    if(record_title == 1){
	        sb.append(line + " ");
	    }
	}
      }

      // write the TreeMap alphabetically (dictionary file)
      StringBuilder dict_sb = new StringBuilder();
      Set<String> keys = map.keySet();
      Iterator<String> iter = keys.iterator();
      //System.out.println("*** Total unique stems: " + keys.size());
      dict_sb.append(keys.size()+"\n");
      int entries = 0;
      while (iter.hasNext()) {
        String key = iter.next();
        //System.out.println(key + " df: " + map.get(key).size());
	dict_sb.append(key+" "+map.get(key).size()+"\n");
	entries = entries + map.get(key).size();
      }
      
      // write the TreeMap alphabetically (dictionary file)
      try (FileWriter dict_writer = new FileWriter("dictionary.txt");
      BufferedWriter dict_bw = new BufferedWriter(dict_writer)) {
          dict_bw.write(dict_sb.toString());    
      } catch (IOException e) {
          System.err.format("IOException: %s%n", e);
      }
      
      
      //display the contents of all Posting Entry ArrayLists (posting file)
      StringBuilder posting_sb = new StringBuilder();
      Iterator<String> PE_iter = keys.iterator();
      //System.out.println("*** Total posting entries: " + entries);
      posting_sb.append(entries+"\n");
      while (PE_iter.hasNext()) {
        String key = PE_iter.next();
	ArrayList<PostingEntry> tempList = map.get(key);
	Iterator list_itr = tempList.iterator();
	while (list_itr.hasNext()){
	    PostingEntry entry =(PostingEntry)list_itr.next();
            //System.out.println(entry.did+" "+entry.tf);
	    posting_sb.append(entry.did+" "+entry.tf+"\n");
	}
      }
      
      // print the posting file
      try (FileWriter posting_writer = new FileWriter("postings.txt");
      BufferedWriter posting_bw = new BufferedWriter(posting_writer)) {
          posting_bw.write(posting_sb.toString());    
      } catch (IOException e) {
          System.err.format("IOException: %s%n", e);
      }
      
      //display the doc collection ArrayList
      StringBuilder doc_sb = new StringBuilder();
      Iterator itr = doc_collection.iterator();
      doc_sb.append(doc_collection.size()+"\n");
      //System.out.println("*** Total docs: " + doc_collection.size());  
      //traverse elements of ArrayList object  
      while(itr.hasNext()){  
          DocID doc =(DocID)itr.next();  
          //System.out.println(doc.docid+" "+doc.title+" "+doc.lineNum);
	  doc_sb.append(doc.docid+" "+doc.lineNum+" "+doc.title+"\n");  
      } 
      
      // print the docids file
      try (FileWriter doc_writer = new FileWriter("docids.txt");
      BufferedWriter doc_bw = new BufferedWriter(doc_writer)) {
          doc_bw.write(doc_sb.toString());    
      } catch (IOException e) {
          System.err.format("IOException: %s%n", e);
      }
     
       
   } 
} 

// A posting entry for a particular stem contains the document id (did - an int), and the term frequency
// we build an ArrayList of posting entries for each stem in the TreeMap
class PostingEntry{
    int did;
    int tf;
    PostingEntry(int did, int tf){
        this.did = did;
	this.tf = tf;
    }
}

// For each document in the collection, we will keep the docid (a string ex. LA018543-48), the title, and starting line number
class DocID{
    String docid;
    String title;
    int lineNum;
    DocID(String docid, String title, int lineNum){
        this.docid = docid;
	this.title = title;
	this.lineNum = lineNum;
    }
}








