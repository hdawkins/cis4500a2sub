CIS4500 F19 A2 
Instructor: Fei Song
Date last modified: Nov 3 2019 
Name: Hillary Dawkins
Student ID: 0717432
email: hdawkins@uoguelph.ca

*****************************************************************************************************

In this directory you will find 3 programs needed for implementing a document retrieval system given 
a tokenized collection of documents (as per A1 output).
PART 1: Preprocessor - this program takes the tokenized input file and completes further preprocessing
including normalization, further filtering, stop word removal, and stemming to produce a stemmed output file.
PART 2: Indexer - this program takes the stemmed text file and produces the inverted index, which is 
described by three output files: dictionary.txt (unique stems and df), postings.txt (did and term freq), 
and docids.txt (docid, line_num, title). See comments in the indexer file for more details.
PART 3: Retriever - this program loads the data from the index files, and implements a document retrieval
system for user-given input queries. For each query (until "quit" or "q"), the similarity value of each
document is computed using the simple inner product (no normalization) and top results are displayed.

You will also find some programs for initial preprocessing (sentence splitting, and tokenization)
carried over from A1 in case you need to start from the original .txt file rather than a tokenized file.

*****************************************************************************************************

To compile: "make" or "make all" in the current directory. Use "make clean" to remove any 
previously compiled files, then "make" or "make all".

To run: 
OPTIONAL (if starting from documents.txt, and producing documents.token). In the current directory:
"java -cp opennlp-tools-1.9.1.jar:. SentenceDetectionEDIT < documents.txt > documents.split" 
"java Scanner < documents.split > documents.token"
produces the tokenized input "documents.token".
This file is already provided in the current directory, so you can start from this point:
"java -cp opennlp-tools-1.9.1.jar:. Preprocessor < documents.token > documents.stem" produces stem file
"java Indexer < documents.stem" produces the index documents (dictionary.txt, postings.txt, docids.txt)
"java -cp opennlp-tools-1.9.1.jar:. Retriever" starts the retrieval system. "quit" or "q" to exit.

To test: You can test that your compiled version is running as expected by using some smaller input
text provided in the testing reference (documents_small.txt) and referring to the provided output
in the TestingReference directory (documents_small.stem, index files). 
Note: Testing on school server (No Machine) on Sunday Nov 3 evening sometimes produced out of memory
errors when using full documents.txt as input. Server was lagging very badly. If this is an issue,
you can test the solution using smaller input documents_small.txt (and documents_small.token) for 
which this program is known to behave correctly. 

*****************************************************************************************************

Testing, limitations, assumptions, and possible improvements:

1. The preprocessing program assumes that input (documents.token) is correctly tokenized using the 
definitions given in A1. It is assumed that the input is formatted using the $DOC, $TITLE, $TEXT
labels and is not robust to formatting errors. This program needs access to the stop words given by a 
text document named "stopwords.txt" The stemmer is done using OpenNLP tools and testing
of closely related words (which I expected to share stems) showed some possible limitations such
as happy and happier not being mapped to the same stem. More investigation would be needed to decide
if they should map to the same stem.

2. The indexer program takes stemmed input which is assumed to use the $DOC, $TITLE, $TEXT formatting
with no errors. Testing was done using a small stemmed input of 5 documents (articles.txt provided as
test case from A1) where unique words could easily be counted using a different method (excel), and 
results compared to the dictionary.txt, posting.txt, and docids.txt output. It was observed that tokens
containing numbers (but not numbers as defined by A1) will show up in the dictionary file. These include 
things like 1-9 (hyphen token) and 14' (apost token). Further filtering could be applied in the future 
to remove these types of number stems that are unlikely to be useful for IR (they generally have df=1). 
Furthermore some contractions split from apost tokens such as 'd and 've appear in the dictionary file.
Consider adding some of these obsesrved contraction stems to stop words file in the future.   

3. The retriever program also needs access to the stop words contained in a text file named "stopwords.txt".
Testing of the computed similarity values was done by using very small input document collections (using
lecture example from slides "to be or not to be" etc) and checking against manual computation. This 
computation assumes log base 2, and computes the similarity using the simple inner product (no normalization).
The normalization could be added in the future. Another limitation is the preprocessing that is applied
to the input query text. The query is "tokenized" by splitting on white-space then applying the same 
preprocessing that occurs in part 1, but it does not go through the same tokenizer that document text does.
As a result, some query terms like "game!" would not be split into "game" plus punctuation, so such a query
term can never be useful since it will never appear in the dictionary file. Further query tokenization rules
could be added in the future. Another future improvement might be to display the titles from the original
document, which would be more readable than the stemmed titles.  




   