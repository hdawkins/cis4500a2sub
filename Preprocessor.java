/*
CIS4500 F19 A2
Name: Hillary Dawkins
Student ID: 0717432
email: hdawkins@uoguelph.ca

PART 1: Preprocessing
This program takes a tokenized input file and conducts further preprocessing including:
Convert to all lower case, remove punctuation and numbers (as defined in A1 tokens), remove stop words (as defined by stopwords.txt)
It is assumed that the input is tokenized correctly as per A1 requirements.
It is assumed that the input file uses the $DOC, $TITLE, $TEXT labels, not robust to formatting errors. 
This file was adapted from the example StemmerDemo file. 
This program needs access to the list of stop words contained in a file named "stopwords.txt"
*/

import java.util.Scanner;
import opennlp.tools.stemmer.PorterStemmer;
import java.util.ArrayList;
import java.util.List;
import java.io.File;


public class Preprocessor { 
   
   public static void main(String args[]) throws Exception{     
      //Create an instance of stemmer
      PorterStemmer stemmer = new PorterStemmer();
      
      //Definitions of words we want to filter (numbers defined as in A1 tokenizer)
      String integers = "[+-]?[0-9]+";
      String reals = "[+-]?[0-9]*.[0-9]+";
      char punctuation_test;
      
      //get a list of stop words, from stopwords.txt
      Scanner stopwords_in = new Scanner(new File ("stopwords.txt"));
      String word = "";
      List<String> stopwords = new ArrayList<String>();
      while (stopwords_in.hasNextLine()){
          word = stopwords_in.nextLine();
	  stopwords.add(word);
      }

      //stemming a file line by line
      Scanner input = new Scanner(System.in);
      while (input.hasNextLine()) {
        String line = input.nextLine();
	// If the line is one of the labels, do not process, print line as it is
	if(line.contains("$DOC") || line.contains("$TEXT") || line.contains("$TITLE")){
	    System.out.print(line);
	    System.out.println();
	}
	else{
	    //otherwise split the line into tokens (split by white-space assuming tokenized input) and process 
	    //first convert line to all lowercase
	    line = line.toLowerCase();
            String[] tokens = line.split("[ \t]+");
	    
	    //for each line, set a flag for whether we need to write a newline for this line
	    //if all tokens in the line are filtered out, and no stems are left to write, do not write a newline
	    int new_line_flag = 0;
	
	    //for every token in the line, check if the token is punctuation, number or stop word	    
            for (int i = 0; i < tokens.length; i++){ 
		 
	        //filter numbers
	        if ((tokens[i].matches(integers))) {
                    continue;
		}
		if(tokens[i].matches(reals)){
		    continue;
		}
		//filter puncutation - punctuation is a single char that is not a letter 
		if (tokens[i].length() == 1){
		    punctuation_test = tokens[i].charAt(0);
		    if(!Character.isLetter(punctuation_test))
		        continue;
		}
		//filter stop words
		if(stopwords.contains(tokens[i])){
		    continue;
		}
		
		//if the token is not a number, punctuation, or stop word convert to stem and print to output
		String stem = stemmer.stem(tokens[i]);
		//observed that some non-numbers (as above) like 140s will become numbers (140) after stemming, do an extra check
		if(!(stem.matches(integers) || stem.matches(reals))){
                    System.out.print(stemmer.stem(tokens[i]) + " ");
		    new_line_flag = 1;
		}
	    }
	    
	    //this will prevent empty line in stems file - avoids empty whitespace dictionary entry
	    if(new_line_flag == 1){
                System.out.println();
	    }
	}
      }
   } 
}      

