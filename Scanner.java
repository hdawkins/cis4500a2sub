/*
CIS4500 F19 A2 (from A1)
Name: Hillary Dawkins
Student ID: 0717432
email: hdawkins@uoguelph.ca

A1 Preprocessing: Tokenization
The scanner reads input one token at a time, where a token is defined by the text.flex file.
It is assumed that the input file is formatted with the $DOC, $TEXT, and $TITLE labels.
When an apostrophized or hyphenated token is encountered, extra parsing takes place.
The parsed tokens are output to the tokenized file. 
For testing: You can uncomment line 174 to print tokens one per line as TYPE(value) to check each type is captured correctly.
This file was adapted from the example Scanner file. 
*/


import java.io.InputStreamReader;

public class Scanner {

    private Lexer scanner = null;
    static public final String WITH_DELIMITER = "((?<=%1$s)|(?=%1$s))";

    //Use the lexer defined by the flex file to scan 
    public Scanner( Lexer lexer ) {
        scanner = lexer; 
    }

    //get next token from the scanner
    public Token getNextToken() throws java.io.IOException {
        return scanner.yylex();
    }
  
    //Given a hyphenated token, check if it follows the rules (keep as hyphen) or not (split)
    // Returns 1 if a hyphenated token should be split, 0 otherwise
    public static int checkHyphen(String tok){
        int split = 0; //do not split
        int numParts;
      
        //assuming that incoming token is hyphenated
	numParts = ((tok).split("-")).length;
	// if it has 2 parts, keep as it is	    
	if(numParts <= 2){
	    //return as it is 
	    split = 0;
	}
	// if it has three parts, check the middle part 
	else if(numParts == 3){
	    //check that the middle word has only 1 or 2 characters
	    if(((tok).split("-")[1]).length() <= 2){
	        //return as it is
		split = 0;
	    }
	    else{
	        //split into individual tokens 
		split = 1;
	    }
	}
	else{
	    //the token has more than 3 parts, return as individual tokens
	    split = 1;
	}   
        return split;
    }

    public static void main(String argv[]) {
    try {
        int numParts;
	int checkHyphenated;
        StringBuilder sb = new StringBuilder();
        Scanner scanner = new Scanner(new Lexer(new InputStreamReader(System.in)));
        Token tok = null;
	
	//keep getting the next token from the scanner, and add it to the ouput string
        while( (tok=scanner.getNextToken()) != null ){
      
        // if the token is of type hyph or apost, do some more parsing
	//hyphen case
	if(tok.m_type == 6){	
	    //If we have a hyphenated token, check if it satisfies the hyphen rules to remain unsplit
	    checkHyphenated = 0;
	    checkHyphenated = checkHyphen(tok.m_value);
	    if(checkHyphenated == 0){
	        //append as it is 
		sb.append(tok.m_value);
		sb.append(" ");
	    }
	    else{
	        //split by hyphens and append each part
	        for (String token : (tok.m_value).split(String.format(WITH_DELIMITER, "-"))) {
		    sb.append(token);
		    sb.append(" ");
		}
	    }
	}
	
	//apost case
	else if(tok.m_type == 5){	
	   checkHyphenated = 0;
	   //first, check if any part is hyphenated - and whether it follows the rules
	   for(String part : tok.m_value.split("'")){
		if(part.contains("-")){
		    checkHyphenated = checkHyphen(tok.m_value);
		    if(checkHyphenated == 1){
		        break;
		    }
	        }
	    }
	    
	    //if any hyphenated part was found not to follow the hypen rules, split the whole token by both "-" and "'"
	    if(checkHyphenated == 1){
	        for(String token : tok.m_value.split(String.format(WITH_DELIMITER,"-|'"))){
		    sb.append(token);
		    sb.append(" ");
		}
	    }
	    
	    // otherwise, check the apost rules
	    else{
	        numParts = ((tok.m_value).split("'")).length;
	        if(numParts == 2){
	            // If there are 2 parts, we need either the first part to be a single character and the second part
		    // to be more than 2 characters, or the last part is 's'
		    if((tok.m_value.split("'")[0].length() == 1 && tok.m_value.split("'")[1].length() > 2) || ((tok.m_value.split("'")[1]).compareToIgnoreCase("s") == 0)){
		        //return as it is 
		        sb.append(tok.m_value);
		        sb.append(" ");
		    }
		    else{
		       //split into tokens, but leave the apostrophe on the second part, if it has one or two characters
		       if(tok.m_value.split("'")[1].length() <= 2){
		           sb.append(tok.m_value.split("'")[0]);
		           sb.append(" ");
		           sb.append("'");
		           sb.append(tok.m_value.split("'")[1]);
		           sb.append(" ");
		       }
		       else{
		           //return as individual tokens
		           for (String token : (tok.m_value).split(String.format(WITH_DELIMITER, "'"))) {
		               sb.append(token);
		               sb.append(" ");
		           }
		       }
		    } 
	        }
	        else if(numParts == 3){
	            //need first part to contain a single character, and the last part to be 's'
		    if(tok.m_value.split("'")[0].length() == 1 && tok.m_value.split("'")[2].compareToIgnoreCase("s") == 0 ){
		        //return as it is 
		        sb.append(tok.m_value);
		        sb.append(" ");
		    }
		    else{
		        //otherwise split into individual tokens
		        for (String token : (tok.m_value).split(String.format(WITH_DELIMITER, "'"))) {
		            sb.append(token);
		            sb.append(" ");
		        }
		    }
	        }
	    }
	}
      
        //all other cases, it is not hyphenated or apostrophized - we append as it is
        else{
            sb.append(tok.m_value); //for getting the value of the token (literal string)
	
	    if((tok.m_type) != 2){
	        sb.append(" "); /*put a white space between all tokens in output - except new line  */
	    }
	}
	
        //System.out.println(tok); //FOR TESTING - outputs as TYPE(value) - one per line
	
      }
      //print the output string that we just built 
      System.out.println(sb);
    }
    catch (Exception e) {
      System.out.println("Unexpected exception:");
      e.printStackTrace();
    }
  }
}
